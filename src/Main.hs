{-# LANGUAGE OverloadedStrings, DeriveGeneric #-}
module Main where

import Control.Applicative (Applicative,pure,(<$>),(<*>))
import Control.Monad (mzero,(=<<),(<=<),(>=>))
import qualified Data.ByteString.Lazy as BS
import qualified Codec.Compression.GZip as GZip
import qualified Data.ByteString.Lazy.Char8 as C
import qualified Data.Text as T
import Data.Aeson
import Data.Aeson.Types
import GHC.Generics
import Data.Maybe (fromJust, isJust)
import Data.List (nub, sort)
import System.Environment (getArgs)


lineContent :: C.ByteString -> [Int]
lineContent = map fromJust . filter isJust . map (parseId . splitElems) . splitLines
    where
        splitLines = C.split '\n'
        splitElems xs = case C.split '\t' xs of 
            [] -> ""
            ys -> last ys
        parseId x = fmap (read . T.unpack) (decodeUserId x) :: Maybe Int

data Action = Action { userId :: T.Text } deriving (Show,Generic)

instance FromJSON Action
instance ToJSON Action

decodeUserId :: C.ByteString -> Maybe T.Text
decodeUserId t = fmap userId (decode t :: Maybe Action) :: Maybe T.Text


main :: IO ()
main = do
    args <- getArgs
    let fileName = head args
    content <- fmap GZip.decompress $ C.readFile fileName
    mapM_ print $ lineContent content
